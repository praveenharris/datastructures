# Linked List Data Structure

A linked list is a linear data structure, in which elements are not stored at contiguous memory locations. The elements in a linked list are linked using pointers as shown in the below image:

![Structure of linked list](https://www.geeksforgeeks.org/wp-content/uploads/gq/2013/03/Linkedlist.png)

In simple words, a linked list consists of nodes where each node contains a data field and a reference(link) to the next node in the list.

##### Topics of discussion
- Singly Linked List 
- Circular Linked List 
- Doubly Linked List
- Misc

### Linked list

**Why Lined List?**

Arrays can be used to store linear data of similar types, but arrays have the following limitations:

1. **The size of an array is fixed.** So we must know the upper limit on the number of elements in advance.
2. **Inserting a new element in an array of elements is too expensive.** e.g inserting a new value into the a sorted array  (all items in the middle have to be shifted), deleting valude from the existing array (all items in the middle have to be shifted) 

**Advantages over arrays**

1. Dynamic size
2. Ease of insertion/deletion

**Drawbacks**

1. Random access not allowed. We have to access each element sequentially starting from the first node. So we cannot do binary serch with linked list.
2. Extra memory space for pointer is required with each element of the list. 
3. Not cache friendly. Since array elements are at contiguous locations, there is locality of reference which is not the case for linked lists.

**Representation**

A linked list is represented by a pointer to the first node of the linked list. The first node is called the head. If the linked list is empty, then the value at the head is NULL.
Each node in the list consists of at least two parts:
1. Data
2. Pointer (or link) to the next node.

**Insertion** 

A node can be added to a linked list in 3 ways:
1. At the front of the linked list - time complexity: **O(1)**
    - if list is empty: add new node
    - create a new node then link the head of the list to it 
2. After a given node - time complexity **O(1)**
    - if node doesn't exist print out a message
    - link left node node to new node and link new node to the original right node. 
3. At the end of the linked list - time complexity **O(1)**
    - if list is empty: add new node
    - create new node then link to tail node
    
**Deletion**

1. Deletion by node: 
    - find previous node of the node to be deleted
    - change link of previous node
2. Deletion by index:
    -  iterate through nodes to the index to be deleted
    - change link of previous node
    

