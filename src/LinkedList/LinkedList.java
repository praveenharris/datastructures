/**
 * A simple Java program to introduce a linked list
 */

package LinkedList;

/* Linked List class */
class LinkedList {

    Node head; // head of list
    Node tail;

    /* Linked list node */
    class Node {
        int data;
        Node next;

        // Constructor to create a new node
        Node (int d) { data = d; next = null; }
    }

    // Inserts a new node at front of the list
    void insertAtHead(int d) {
        Node newNode = new Node(d);
        if (head == null) {
            head = newNode;
            tail = newNode;
            return;
        }

        newNode.next = head;
        head = newNode;
    }

    // Insert a new node at the end of a list
    void insertAtTail(int d) {
        Node newNode = new Node(d);
        if (head == null) {
            head = newNode;
            tail = newNode;
            return;
        }

        tail.next = newNode;
        tail = newNode;
    }

    // Insert new node after the given node
    void insertAfter(Node prevNode, int d) {
        Node newNode = new Node(d);
        if (prevNode == null) {
            System.out.println("Given node is null");
            return;
        } else if (prevNode == tail) {
            insertAtTail(d);
        } else {
            newNode.next = prevNode.next;
            prevNode.next= newNode;
        }

    }

    // delete first occurrence of key in the list
    void deleteNodeWithKey(int key) {
        Node iteratingNode = head, previousNode = null;

        if (iteratingNode != null && iteratingNode.data == key) {
            head = iteratingNode.next;
            if (head.next == null) {
                tail = head;
            }
            return;
        }

        while (iteratingNode != null && iteratingNode.data != key) {
            previousNode = iteratingNode;
            iteratingNode = iteratingNode.next;
        }

        if (iteratingNode != null) {
            previousNode.next = iteratingNode.next;
            if (iteratingNode.next == null) {
                tail = previousNode;
            }
        }

    }

    // delete node at given index in the list
    void deleteNodeWithIndex(int position) {
        if (head == null) {
            return;
        }

        Node iteratingNode = head, previousNode = null;
        if (position == 0) {
            deleteNodeWithKey(iteratingNode.data);
            return;
        }

        while (position > 0) {
            position -= 1;
            previousNode = iteratingNode;
            iteratingNode = iteratingNode.next;
        }

        if (iteratingNode.next == null) {
            previousNode.next = null;
            tail = previousNode;
        }else {
            previousNode.next = iteratingNode.next;
        }

    }

    // This function prints contents of linked list starting from head
    void printList() {
        Node n = head;
        while(n != null) {
            System.out.print(n.data + " ");
            n = n.next;
        }
    }

    /* Method to create a simple linked list with 3 nodes */
    public static void main(String[] args) {

        // Start with the empty list
        LinkedList llist = new LinkedList();

        llist.insertAtHead(3);
        llist.insertAtHead(1);
        llist.insertAtTail(5);
        llist.insertAtTail(6);
        llist.insertAtTail(7);
        llist.insertAfter(llist.head, 2);
        llist.insertAfter(llist.head.next.next, 4);
        llist.insertAtHead(0);
        llist.insertAfter(llist.head.next.next.next.next.next.next.next, 8);
        System.out.println("Original linked list:");
        llist.printList();

        llist.deleteNodeWithKey(0);
        llist.deleteNodeWithKey(5);
        llist.deleteNodeWithKey(8);
        System.out.println("\nLinked list after deletions(key):");
        llist.printList();

        llist.deleteNodeWithIndex(5);
        llist.deleteNodeWithIndex(3);
        llist.deleteNodeWithIndex(0);
        System.out.println("\nLinked list after deletions(index):");
        llist.printList();

        System.out.println("\n\n" + llist.head.data + " " + llist.tail.data);

    }
}
